/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadorajava;

import java.awt.Color;
import java.awt.Frame;
import java.util.HashSet;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.swing.ImageIcon;
import javax.swing.JButton;


public class Calculadora extends javax.swing.JFrame {

    //Usaremos el motor de JavaScript, haremos una instancia de este mismo.
    //importamos estos metodos para usarlos posteriormente en el evento de resultado e ir haciendo los calculos a tiempo real.
    ScriptEngineManager sem = new ScriptEngineManager();
    ScriptEngine se = sem.getEngineByName("JavaScript");
    public Calculadora() {
        initComponents();
        //Colocamos el frame en el medio de la pantalla
        setLocationRelativeTo(null);
        
        
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        textoOperacion = new javax.swing.JLabel();
        textoResultado = new javax.swing.JLabel();
        interruptor = new javax.swing.JButton();
        botonCerrar = new javax.swing.JLabel();
        botonMinimizar = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        botonResultado = new javax.swing.JButton();
        botonNueve = new javax.swing.JButton();
        botonDEL = new javax.swing.JButton();
        botonPorcentaje = new javax.swing.JButton();
        botonDiv = new javax.swing.JButton();
        botonMulti = new javax.swing.JButton();
        botonRestar = new javax.swing.JButton();
        botonSum = new javax.swing.JButton();
        botonC = new javax.swing.JButton();
        botonSiete = new javax.swing.JButton();
        botonOcho = new javax.swing.JButton();
        botonSeis = new javax.swing.JButton();
        botonCuatro = new javax.swing.JButton();
        botonCinco = new javax.swing.JButton();
        botonTres = new javax.swing.JButton();
        botonUno = new javax.swing.JButton();
        botonDos = new javax.swing.JButton();
        botonPunto = new javax.swing.JButton();
        botonCero = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(244, 253, 251));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        textoOperacion.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        textoOperacion.setForeground(new java.awt.Color(55, 62, 71));
        textoOperacion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jPanel1.add(textoOperacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 316, 44));

        textoResultado.setFont(new java.awt.Font("Dialog", 1, 48)); // NOI18N
        textoResultado.setForeground(new java.awt.Color(55, 62, 71));
        textoResultado.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jPanel1.add(textoResultado, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, 316, 90));

        interruptor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/darkmode_1.png"))); // NOI18N
        interruptor.setFocusPainted(false);
        interruptor.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        interruptor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                interruptorActionPerformed(evt);
            }
        });
        jPanel1.add(interruptor, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 10, 30, 20));

        botonCerrar.setFont(new java.awt.Font("Dialog", 1, 20)); // NOI18N
        botonCerrar.setForeground(new java.awt.Color(255, 0, 0));
        botonCerrar.setText(" •");
        botonCerrar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                botonCerrarMouseClicked(evt);
            }
        });
        jPanel1.add(botonCerrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 20, -1));

        botonMinimizar.setFont(new java.awt.Font("Dialog", 1, 20)); // NOI18N
        botonMinimizar.setForeground(new java.awt.Color(255, 153, 0));
        botonMinimizar.setText(" •");
        botonMinimizar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                botonMinimizarMouseClicked(evt);
            }
        });
        jPanel1.add(botonMinimizar, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 20, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 340, 150));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        botonResultado.setBackground(new java.awt.Color(255, 255, 255));
        botonResultado.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        botonResultado.setForeground(new java.awt.Color(255, 255, 255));
        botonResultado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn3.png"))); // NOI18N
        botonResultado.setText("=");
        botonResultado.setFocusPainted(false);
        botonResultado.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonResultado.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonResultado.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1_pressed.png"))); // NOI18N
        botonResultado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonResultadoActionPerformed(evt);
            }
        });
        jPanel2.add(botonResultado, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 340, 50, 50));

        botonNueve.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        botonNueve.setForeground(new java.awt.Color(55, 62, 71));
        botonNueve.setIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn2.png"))); // NOI18N
        botonNueve.setText("9");
        botonNueve.setFocusPainted(false);
        botonNueve.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonNueve.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonNueve.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1_pressed.png"))); // NOI18N
        botonNueve.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonNueveActionPerformed(evt);
            }
        });
        jPanel2.add(botonNueve, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 100, 50, 50));

        botonDEL.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        botonDEL.setForeground(new java.awt.Color(55, 62, 71));
        botonDEL.setIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonDEL.setText("←");
        botonDEL.setFocusPainted(false);
        botonDEL.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonDEL.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonDEL.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1_pressed.png"))); // NOI18N
        botonDEL.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonDELActionPerformed(evt);
            }
        });
        jPanel2.add(botonDEL, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 20, 50, 50));

        botonPorcentaje.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        botonPorcentaje.setForeground(new java.awt.Color(55, 62, 71));
        botonPorcentaje.setIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonPorcentaje.setText("%");
        botonPorcentaje.setFocusPainted(false);
        botonPorcentaje.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonPorcentaje.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonPorcentaje.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1_pressed.png"))); // NOI18N
        botonPorcentaje.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonPorcentajeActionPerformed(evt);
            }
        });
        jPanel2.add(botonPorcentaje, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 20, 50, 50));

        botonDiv.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        botonDiv.setForeground(new java.awt.Color(55, 62, 71));
        botonDiv.setIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonDiv.setText("/");
        botonDiv.setFocusPainted(false);
        botonDiv.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonDiv.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonDiv.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1_pressed.png"))); // NOI18N
        botonDiv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonDivActionPerformed(evt);
            }
        });
        jPanel2.add(botonDiv, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 20, 50, 50));

        botonMulti.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        botonMulti.setForeground(new java.awt.Color(55, 62, 71));
        botonMulti.setIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonMulti.setText("X");
        botonMulti.setFocusPainted(false);
        botonMulti.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonMulti.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonMulti.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1_pressed.png"))); // NOI18N
        botonMulti.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonMultiActionPerformed(evt);
            }
        });
        jPanel2.add(botonMulti, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 100, 50, 50));

        botonRestar.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        botonRestar.setForeground(new java.awt.Color(55, 62, 71));
        botonRestar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonRestar.setText("-");
        botonRestar.setFocusPainted(false);
        botonRestar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonRestar.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonRestar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1_pressed.png"))); // NOI18N
        botonRestar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonRestarActionPerformed(evt);
            }
        });
        jPanel2.add(botonRestar, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 180, 50, 50));

        botonSum.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        botonSum.setForeground(new java.awt.Color(55, 62, 71));
        botonSum.setIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonSum.setText("+");
        botonSum.setFocusPainted(false);
        botonSum.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonSum.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonSum.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1_pressed.png"))); // NOI18N
        botonSum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonSumActionPerformed(evt);
            }
        });
        jPanel2.add(botonSum, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 260, 50, 50));

        botonC.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        botonC.setForeground(new java.awt.Color(55, 62, 71));
        botonC.setIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonC.setText("C");
        botonC.setFocusPainted(false);
        botonC.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonC.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonC.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1_pressed.png"))); // NOI18N
        botonC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCActionPerformed(evt);
            }
        });
        jPanel2.add(botonC, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 50, 50));

        botonSiete.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        botonSiete.setForeground(new java.awt.Color(55, 62, 71));
        botonSiete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn2.png"))); // NOI18N
        botonSiete.setText("7");
        botonSiete.setFocusPainted(false);
        botonSiete.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonSiete.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonSiete.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1_pressed.png"))); // NOI18N
        botonSiete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonSieteActionPerformed(evt);
            }
        });
        jPanel2.add(botonSiete, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, 50, 50));

        botonOcho.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        botonOcho.setForeground(new java.awt.Color(55, 62, 71));
        botonOcho.setIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn2.png"))); // NOI18N
        botonOcho.setText("8");
        botonOcho.setFocusPainted(false);
        botonOcho.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonOcho.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonOcho.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1_pressed.png"))); // NOI18N
        botonOcho.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonOchoActionPerformed(evt);
            }
        });
        jPanel2.add(botonOcho, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 100, 50, 50));

        botonSeis.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        botonSeis.setForeground(new java.awt.Color(55, 62, 71));
        botonSeis.setIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn2.png"))); // NOI18N
        botonSeis.setText("6");
        botonSeis.setFocusPainted(false);
        botonSeis.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonSeis.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonSeis.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1_pressed.png"))); // NOI18N
        botonSeis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonSeisActionPerformed(evt);
            }
        });
        jPanel2.add(botonSeis, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 180, 50, 50));

        botonCuatro.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        botonCuatro.setForeground(new java.awt.Color(55, 62, 71));
        botonCuatro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn2.png"))); // NOI18N
        botonCuatro.setText("4");
        botonCuatro.setFocusPainted(false);
        botonCuatro.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonCuatro.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonCuatro.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1_pressed.png"))); // NOI18N
        botonCuatro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCuatroActionPerformed(evt);
            }
        });
        jPanel2.add(botonCuatro, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, 50, 50));

        botonCinco.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        botonCinco.setForeground(new java.awt.Color(55, 62, 71));
        botonCinco.setIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn2.png"))); // NOI18N
        botonCinco.setText("5");
        botonCinco.setFocusPainted(false);
        botonCinco.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonCinco.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonCinco.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1_pressed.png"))); // NOI18N
        botonCinco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCincoActionPerformed(evt);
            }
        });
        jPanel2.add(botonCinco, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 180, 50, 50));

        botonTres.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        botonTres.setForeground(new java.awt.Color(55, 62, 71));
        botonTres.setIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn2.png"))); // NOI18N
        botonTres.setText("3");
        botonTres.setFocusPainted(false);
        botonTres.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonTres.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonTres.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1_pressed.png"))); // NOI18N
        botonTres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonTresActionPerformed(evt);
            }
        });
        jPanel2.add(botonTres, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 260, 50, 50));

        botonUno.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        botonUno.setForeground(new java.awt.Color(55, 62, 71));
        botonUno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn2.png"))); // NOI18N
        botonUno.setText("1");
        botonUno.setFocusPainted(false);
        botonUno.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonUno.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonUno.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1_pressed.png"))); // NOI18N
        botonUno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonUnoActionPerformed(evt);
            }
        });
        jPanel2.add(botonUno, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 260, 50, 50));

        botonDos.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        botonDos.setForeground(new java.awt.Color(55, 62, 71));
        botonDos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn2.png"))); // NOI18N
        botonDos.setText("2");
        botonDos.setFocusPainted(false);
        botonDos.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonDos.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonDos.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1_pressed.png"))); // NOI18N
        botonDos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonDosActionPerformed(evt);
            }
        });
        jPanel2.add(botonDos, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 260, 50, 50));

        botonPunto.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        botonPunto.setForeground(new java.awt.Color(55, 62, 71));
        botonPunto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn2.png"))); // NOI18N
        botonPunto.setText(".");
        botonPunto.setFocusPainted(false);
        botonPunto.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonPunto.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonPunto.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1_pressed.png"))); // NOI18N
        jPanel2.add(botonPunto, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 340, 50, 50));

        botonCero.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        botonCero.setForeground(new java.awt.Color(55, 62, 71));
        botonCero.setIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn2.png"))); // NOI18N
        botonCero.setText("0");
        botonCero.setFocusPainted(false);
        botonCero.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonCero.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1.png"))); // NOI18N
        botonCero.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/calculadorajava/images/btn1_pressed.png"))); // NOI18N
        jPanel2.add(botonCero, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 340, 50, 50));

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 150, 340, 400));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCActionPerformed
        //añadimos un texto nulo a los dos cuadros de texto para que al presionar el boton se borre el contenido.
        textoOperacion.setText("");
        textoResultado.setText("");
    }//GEN-LAST:event_botonCActionPerformed

    private void botonSieteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonSieteActionPerformed
        //pasamos el numero del boton por el parametro para que se añada en el texto de la operacion
        addNumber("7");
        botonResultado.doClick();
    }//GEN-LAST:event_botonSieteActionPerformed

    private void botonOchoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonOchoActionPerformed
        //pasamos el numero del boton por el parametro para que se añada en el texto de la operacion
        addNumber("8");
        botonResultado.doClick();
    }//GEN-LAST:event_botonOchoActionPerformed

    private void botonNueveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonNueveActionPerformed
        //pasamos el numero del boton por el parametro para que se añada en el texto de la operacion
        addNumber("9");
        botonResultado.doClick();
    }//GEN-LAST:event_botonNueveActionPerformed

    private void botonCuatroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCuatroActionPerformed
        //pasamos el numero del boton por el parametro para que se añada en el texto de la operacion
        addNumber("4");
        botonResultado.doClick();
    }//GEN-LAST:event_botonCuatroActionPerformed

    private void botonCincoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCincoActionPerformed
        //pasamos el numero del boton por el parametro para que se añada en el texto de la operacion
        addNumber("5");
        botonResultado.doClick();
    }//GEN-LAST:event_botonCincoActionPerformed

    private void botonSeisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonSeisActionPerformed
       //pasamos el numero del boton por el parametro para que se añada en el texto de la operacion
        addNumber("6");
        botonResultado.doClick();
    }//GEN-LAST:event_botonSeisActionPerformed

    private void botonUnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonUnoActionPerformed
       //pasamos el numero del boton por el parametro para que se añada en el texto de la operacion
        addNumber("1");
        botonResultado.doClick();
    }//GEN-LAST:event_botonUnoActionPerformed

    private void botonDosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonDosActionPerformed
        //pasamos el numero del boton por el parametro para que se añada en el texto de la operacion
        addNumber("2");
        botonResultado.doClick();
    }//GEN-LAST:event_botonDosActionPerformed

    private void botonTresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonTresActionPerformed
        //pasamos el numero del boton por el parametro para que se añada en el texto de la operacion
        addNumber("3");
        botonResultado.doClick();
    }//GEN-LAST:event_botonTresActionPerformed

    private void botonDELActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonDELActionPerformed
        //pasamos el numero del boton por el parametro para que se añada en el texto de la operacion
        String texto = textoOperacion.getText().substring(0,textoOperacion.getText().length()-1);
        textoOperacion.setText(texto);
        botonResultado.doClick();
        
    }//GEN-LAST:event_botonDELActionPerformed

    private void botonPorcentajeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonPorcentajeActionPerformed
        //pasamos el numero del boton por el parametro para que se añada en el texto de la operacion
        addNumber("%");
    }//GEN-LAST:event_botonPorcentajeActionPerformed

    private void botonDivActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonDivActionPerformed
        //pasamos el numero del boton por el parametro para que se añada en el texto de la operacion
        addNumber("/");
    }//GEN-LAST:event_botonDivActionPerformed

    private void botonMultiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonMultiActionPerformed
       //pasamos el numero del boton por el parametro para que se añada en el texto de la operacion
       addNumber("*");
    }//GEN-LAST:event_botonMultiActionPerformed

    private void botonRestarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonRestarActionPerformed
      //pasamos el numero del boton por el parametro para que se añada en el texto de la operacion
      addNumber("-");
    }//GEN-LAST:event_botonRestarActionPerformed

    private void botonSumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonSumActionPerformed
       //pasamos el numero del boton por el parametro para que se añada en el texto de la operacion
       addNumber("+");
       
    }//GEN-LAST:event_botonSumActionPerformed

    private void botonResultadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonResultadoActionPerformed
        try{
            textoResultado.setText(se.eval(textoOperacion.getText()).toString());
            
        }catch(Exception e){
            //botonC.doClick();
        }
    }//GEN-LAST:event_botonResultadoActionPerformed
    //este es un booleano que se encargará de hacernos saber en que modo se encuentra la ventana
    boolean modoOscuroSwitch = false;
    private void interruptorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_interruptorActionPerformed
        if(!modoOscuroSwitch){
        jPanel1.setBackground(Color.decode("#212b41"));
        jPanel2.setBackground(Color.decode("#2e3951"));
        textoOperacion.setForeground(Color.decode("#0db387"));
        textoResultado.setForeground(Color.decode("#0db387"));
        //metodo para cambiar el color a los numeros.
        cambiarColorNumericos(botonUno);
        cambiarColorNumericos(botonDos);
        cambiarColorNumericos(botonTres);
        cambiarColorNumericos(botonCuatro);
        cambiarColorNumericos(botonCinco);
        cambiarColorNumericos(botonSeis);
        cambiarColorNumericos(botonSiete);
        cambiarColorNumericos(botonOcho);
        cambiarColorNumericos(botonNueve);
        cambiarColorNumericos(botonCero);
        cambiarColorNumericos(botonPunto);
        //metodo para cambiar el color a las operaciones
        cambiarColoresOperaciones(botonC);
        cambiarColoresOperaciones(botonDEL);
        cambiarColoresOperaciones(botonPorcentaje);
        cambiarColoresOperaciones(botonSum);
        cambiarColoresOperaciones(botonDiv);
        cambiarColoresOperaciones(botonRestar);
        cambiarColoresOperaciones(botonMulti);
        //cambiamos manualmente el color del boton resultado .
        interruptor.setIcon(new ImageIcon(getClass().getResource("images/darkmode_2.png")));
        botonResultado.setIcon(new ImageIcon(getClass().getResource("images/btn3_dark.png")));
        botonResultado.setPressedIcon(new ImageIcon(getClass().getResource("images/btn3_dark.png")));
        botonResultado.setRolloverIcon(new ImageIcon(getClass().getResource("images/btn3_pressed_dark.png")));
        botonResultado.setForeground(Color.decode("#2e3951"));
        //invertimos el valor del booleano para que se active de nuevo la próxima vez.
        modoOscuroSwitch =!modoOscuroSwitch;
        }else{
            Calculadora frame = new Calculadora();
            this.dispose();
            frame.setVisible(true);
        }
        
    }//GEN-LAST:event_interruptorActionPerformed

    private void botonCerrarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonCerrarMouseClicked
        //boton de cerrar
        this.dispose();
    }//GEN-LAST:event_botonCerrarMouseClicked

    private void botonMinimizarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonMinimizarMouseClicked
        //boton Minimizar
        this.setState(Frame.ICONIFIED);
    }//GEN-LAST:event_botonMinimizarMouseClicked

   
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Calculadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Calculadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Calculadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Calculadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Calculadora().setVisible(true);
            }
        });
    }
    //Creamos un metodo para añadir colores, características e iconos a los botones de operaciones
    public void cambiarColoresOperaciones(JButton boton){
        boton.setIcon(new ImageIcon(getClass().getResource("images/btn1_dark.png")));
        boton.setPressedIcon(new ImageIcon(getClass().getResource("images/btn1_dark.png")));
        boton.setRolloverIcon(new ImageIcon(getClass().getResource("images/btn1_pressed_dark.png")));
        boton.setForeground(Color.decode("#0db387"));
    }
    //Creamos un metodo para añadir colores, características y iconos de los botones
    public void cambiarColorNumericos(JButton boton){
        boton.setIcon(new ImageIcon(getClass().getResource("images/btn2_dark.png")));
        boton.setPressedIcon(new ImageIcon(getClass().getResource("images/btn2_dark.png")));
        boton.setRolloverIcon(new ImageIcon(getClass().getResource("images/btn1_pressed_dark.png")));
        boton.setForeground(Color.decode("#96a8a0"));

    }
    //Creamos un metodo para añadir los numeros en el texto de la operación
    public void addNumber(String digito){
        //añadimos lo que hay actualmente en el texto + el digito pasado por parametro.
        textoOperacion.setText(textoOperacion.getText()+digito);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonC;
    private javax.swing.JButton botonCero;
    private javax.swing.JLabel botonCerrar;
    private javax.swing.JButton botonCinco;
    private javax.swing.JButton botonCuatro;
    private javax.swing.JButton botonDEL;
    private javax.swing.JButton botonDiv;
    private javax.swing.JButton botonDos;
    private javax.swing.JLabel botonMinimizar;
    private javax.swing.JButton botonMulti;
    private javax.swing.JButton botonNueve;
    private javax.swing.JButton botonOcho;
    private javax.swing.JButton botonPorcentaje;
    private javax.swing.JButton botonPunto;
    private javax.swing.JButton botonRestar;
    private javax.swing.JButton botonResultado;
    private javax.swing.JButton botonSeis;
    private javax.swing.JButton botonSiete;
    private javax.swing.JButton botonSum;
    private javax.swing.JButton botonTres;
    private javax.swing.JButton botonUno;
    private javax.swing.JButton interruptor;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel textoOperacion;
    private javax.swing.JLabel textoResultado;
    // End of variables declaration//GEN-END:variables
}
