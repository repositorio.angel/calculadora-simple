# Calculadora Simple

Calculadora Java para principiantes

![Calculator Light](capturas/blanco.PNG) ![Calculator Dark](capturas/negro.PNG)

Hoy os presento mi proyecto, una clalculadora sencilla en java pero con ciertas complicaciones como, darkmode y light mode , resultado instantaneo, importar un motor de JavaScript y mucho más.

## Features

* Buen diseño y funcionalidad
* Light and dark mode.
* Diferentes tipos de operaciones simultaneas.
* Resultado conforme escribes.

## Getting Started

1. Descarga el repositorio completo.
2. Importalo en NETBEANS o en cualquier otro tipo de IDE
3. Compile and run :)


## Thanks

Gracias por visitar y seguir el manual, recordar que también tenemos PL/SQL y Sistemas operativos.
